import "package:flutter/material.dart";
import "package:flutter_slidable/flutter_slidable.dart";
import "package:intl/intl.dart";
import "package:scoped_model/scoped_model.dart";
import 'package:swipe_to/swipe_to.dart';
import "TasksDBWorker.dart";
import "TasksModel.dart" show Task, TasksModel, tasksModel;


/// ****************************************************************************
/// The Tasks List sub-screen.
/// ****************************************************************************
class TasksList extends StatelessWidget {


  /// The build() method.
  ///
  /// @param  inContext The BuildContext for this widget.
  /// @return           A Widget.
  Widget build(BuildContext inContext) {

    print("## TasksList.build()");
    print("## tasksModel.entityList: ${tasksModel.entityList}");

    // Return widget.
    return ScopedModel<TasksModel>(
        model : tasksModel,
        child : ScopedModelDescendant<TasksModel>(
            builder : (BuildContext inContext, Widget? inChild, TasksModel inModel) {
              return Scaffold(
                // Add note.
                  floatingActionButton : FloatingActionButton(
                      child : Icon(Icons.add, color : Colors.white),
                      onPressed : () async {
                        tasksModel.entityBeingEdited = Task();
                        tasksModel.setChosenDate(null);
                        //tasksModel.setColor(null);
                        tasksModel.setStackIndex(1);
                      }
                  ),
                  body : ListView.builder(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      itemCount : tasksModel.entityList.length,
                      itemBuilder : (BuildContext inBuildContext, int inIndex) {
                        Task task = tasksModel.entityList[inIndex];
                        String sDueDate = '';

                        if (task.dueDate != null) {
                          List<String>? dateParts = task.dueDate?.split(",");
                          if (dateParts != null && dateParts.length >= 3) {
                            DateTime dueDate = DateTime(
                              int.parse(dateParts[0]),
                              int.parse(dateParts[1]),
                              int.parse(dateParts[2]),
                            );
                            sDueDate = DateFormat.yMMMMd("ru_RU").format(dueDate.toLocal());
                          }
                        }
                        return Slidable(
                            key: const ValueKey(0),
                            startActionPane: ActionPane(
                              motion: const ScrollMotion(),
                              //dismissible: DismissiblePane(onDismissed: () {}),
                              children: [
                                SlidableAction(
                                  onPressed: (BuildContext context) {
                                    _deleteTask(context, task, ScaffoldMessenger.of(inContext));
                                  },
                                  backgroundColor: Color(0xFFFE4A49),
                                  foregroundColor: Colors.white,
                                  icon: Icons.delete,
                                  label: 'Удалить',
                                ),
                              ],
                            ),
                            child : ListTile(
                                leading : Checkbox(
                                    value : task.completed == "true" ? true : false,
                                    onChanged : (inValue) async {
                                      // Update the completed value for this task and refresh the list.
                                      task.completed = inValue.toString();
                                      await TasksDBWorker.db.update(task);
                                      tasksModel.loadData("tasks", TasksDBWorker.db);
                                    }
                                ),
                                title : Text(
                                    "${task.description}",
                                    // Dim and strikethrough the text when the task is completed.
                                    style : task.completed == "true" ?
                                    TextStyle(color : Theme.of(inContext).disabledColor, decoration : TextDecoration.lineThrough) :
                                    TextStyle(color : Theme.of(inContext).textTheme.headline1?.color)
                                ),
                                subtitle : task.dueDate == null ?
                                null :
                                Text(
                                    sDueDate,
                                    // Dim and strikethrough the text when the task is completed.
                                    style : task.completed == "true" ?
                                    TextStyle(color : Theme.of(inContext).disabledColor, decoration : TextDecoration.lineThrough)
                                        :
                                    TextStyle(color : Theme.of(inContext).textTheme.headline1?.color)
                                ),
                                // Edit existing task.
                                onTap : () async {
                                  // Can't edit a completed task.
                                  if (task.completed == "true") { return; }
                                  // Get the data from the database and send to the edit view.
                                  tasksModel.entityBeingEdited = await TasksDBWorker.db.get(task.id!);
                                  // Parse out the due date, if any, and set it in the model for display.
                                  if (tasksModel.entityBeingEdited.dueDate == null) {
                                    tasksModel.setChosenDate(null);
                                  } else {
                                    tasksModel.setChosenDate(sDueDate);
                                  }
                                  tasksModel.setStackIndex(1);
                                }
                            ),
                        );
                      } /* End itemBuilder. */
                  ) /* End End ListView.builder. */
              ); /* End Scaffold. */
            } /* End ScopedModelDescendant builder. */
        ) /* End ScopedModelDescendant. */
    ); /* End ScopedModel. */

  } /* End build(). */

  /// Show a dialog requesting delete confirmation.
  ///
  /// @param  inContext The BuildContext of the parent Widget.
  /// @param  inNote    The note (potentially) being deleted.
  /// @return           Future.
  Future _deleteTask(BuildContext inContext, Task inTask, ScaffoldMessengerState scaffoldMessenger) async {
    print("## TasksList._deleteNote(): inNote = $inTask");
    return showDialog(
        context : inContext,
        barrierDismissible : false,
        builder : (BuildContext inAlertContext) {
          return AlertDialog(
              title : Text("Удалить заметку"),
              content : Text("Вы уверены, что хотите удалить ${inTask.description}?"),
              actions : [
                TextButton(
                  child: Text("Отменить"),
                  onPressed: () {
                    // Просто скрываем диалоговое окно.
                    Navigator.of(inAlertContext).pop();
                  },
                ),
                ElevatedButton(
                  child: Text("Удалить"),
                  onPressed: () async {
                    // Удаление из базы данных, затем скрытие диалогового окна, отображение SnackBar, а затем повторная загрузка данных для списка.
                    await TasksDBWorker.db.delete(inTask.id!);
                    Navigator.of(inAlertContext).pop();
                    scaffoldMessenger.showSnackBar(
                    //ScaffoldMessenger.of(inContext).showSnackBar(
                      SnackBar(
                        backgroundColor: Colors.red,
                        duration: Duration(seconds: 2),
                        content: Text("Заметка удалена"),
                      ),
                    );
                    // Повторная загрузка данных из базы данных для обновления списка.
                    tasksModel.loadData("tasks", TasksDBWorker.db);
                  },
                )
              ]
          );
        }
    );

  } /* End _deleteNote(). */

} /* End class. */