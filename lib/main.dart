import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'notes/Notes.dart';
import 'tasks/Tasks.dart';
import 'utils.dart' as utils;

late Directory docsDir;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  startMeUp() async {
    docsDir = await getApplicationDocumentsDirectory();
    utils.docsDir = docsDir;

    // Инициализация данных для русской локали
    await initializeDateFormatting('ru_RU', null);

    runApp(FlutterBook());
  }
  startMeUp();
}

class FlutterBook extends StatelessWidget {

  Widget build(BuildContext inContext) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        DefaultCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('ru', 'RU'),
      ],
      home: DefaultTabController(
        length: 2, //4
        child: Scaffold(
          appBar: AppBar(
            title: Text('Записная книжка'),
            bottom: TabBar(
              tabs: [
                //Tab(icon: Icon(Icons.date_range), text: 'Встречи'),
                //Tab(icon: Icon(Icons.contacts), text: 'Контакты'),
                Tab(icon: Icon(Icons.note), text: 'Заметки'),
                Tab(icon: Icon(Icons.assignment_turned_in), text: 'Задачи'),
              ]
            )
          ),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(), // Запретить прокрутку между разделами
            children: [
              //Appointments(),
              //Contacts(),
              Notes(),
              Tasks()
            ],
          ),
        )
      )
    );
  }
}


